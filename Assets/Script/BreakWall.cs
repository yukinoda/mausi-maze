﻿using UnityEngine;
using System.Collections;

public class BreakWall : MonoBehaviour {

    public Transform Pick;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Stone")
        {
            Destroy(gameObject);
        }
    }
}
