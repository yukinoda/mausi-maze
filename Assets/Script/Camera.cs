﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour {

	public GameObject player;
	//public Transform target;
	private Vector3 offset;

	// Use this for initialization
	void Start() {
		offset = transform.position - player.transform.position;
	}

	void Update(){
		//Vector3 relativePos = target.position - transform.position;
		//Quaternion rotation = Quaternion.LookRotation(relativePos);
		//transform.rotation = rotation;
	}

	void LateUpdate(){
		transform.position = player.transform.position + offset;
	}
}
