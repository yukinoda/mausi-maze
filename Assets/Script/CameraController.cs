﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform target;
	public float lookSmooth = 0.09f;
	public Vector3 offsetFromTarget = new Vector3(0, 6, -8);
	public float xTilt = 10;

	Vector3 destination = Vector3.zero;
	CharacterController charController;
	float rotateVel = 0;

	void Start () {
		SetCameraTarget (target);
	}

	void SetCameraTarget(Transform t) {
		target = t;

		if (target != null) {
			if (target.GetComponent<CharacterController> ()) {
				charController = target.GetComponent<CharacterController> ();
			} else {
				Debug.LogError ("The cameras target nedds a character controller");
			}
		} else {
			Debug.LogError ("The camera needs a target"); 
		}
	}

	void LateUpdate() {
		MoveToTarget();
		LookAtTarget();

	}

	void MoveToTarget() {
		destination = charController.TargetRotation * offsetFromTarget;
		destination += target.position;
		transform.position = destination;
	}

	void LookAtTarget() {
		float eulerYAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y, ref rotateVel, lookSmooth);
		transform.rotation = Quaternion.Euler (transform.eulerAngles.x, eulerYAngle, 0);
	}

	void CompensateForWalls (Vector3 fromObject, ref Vector3 toTarget) {
		Debug.DrawLine (fromObject, toTarget, Color.cyan);
		RaycastHit wallHit = new RaycastHit ();
		if (Physics.Linecast (fromObject, toTarget, out wallHit)) {
			Debug.DrawLine (wallHit.point, Vector3.left, Color.red);
			toTarget = new Vector3 (wallHit.point.x, toTarget.y, wallHit.point.z);
		}
	}
}
