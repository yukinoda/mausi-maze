﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {
	
	public float inputDelay = 0.1f;
	public float forwardVel = 12;
	public float rotateVel = 100;
	public Animator anim;

	Quaternion targetRotation;
	Rigidbody rBody;
	float forwardInput, turnInput;

	public Quaternion TargetRotation {
		get {return targetRotation;}
	}

	void Start() {
		anim = GetComponent<Animator> ();
		targetRotation = transform.rotation;
		if (GetComponent<Rigidbody> ()) {
			rBody = GetComponent<Rigidbody> ();
		} else {
			Debug.LogError ("The Character needs as Rigidbody.");
			forwardInput = turnInput = 0;
		}
	}

	void GetInput() {
		forwardInput = Input.GetAxis ("Vertical");
		turnInput = Input.GetAxis ("Horizontal");

	}

	void OnCollisionEnter() {
		rBody.velocity = Vector3.zero;
		rBody.angularVelocity = Vector3.zero;
	} 
		
	void Update() {
		GetInput();
		Turn();
	}

	void FixedUpdate() {
		Run();
	}

	void Run() {
		if (Mathf.Abs (forwardInput) > inputDelay) {
			rBody.velocity = transform.forward * forwardInput * forwardVel;
			anim.Play("Walk");
		} else {
			rBody.velocity = Vector3.zero;
			rBody.angularVelocity = Vector3.zero;
			anim.Play("Idle");
		}
	}

	void Turn() {
		if (Mathf.Abs (turnInput) > inputDelay) {
			targetRotation *= Quaternion.AngleAxis (rotateVel * turnInput * Time.deltaTime, Vector3.up);
		}
		transform.rotation = targetRotation;
	}
}
