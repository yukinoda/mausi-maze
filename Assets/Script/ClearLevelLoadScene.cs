﻿using UnityEngine;
using System.Collections;

public class ClearLevelLoadScene : MonoBehaviour {
    void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			Application.LoadLevel (3);
		}
	}
}
