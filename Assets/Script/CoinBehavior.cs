﻿using UnityEngine;
using System.Collections;

public class CoinBehavior : MonoBehaviour {
       GameManager GMS;
       private float RotateSpeed = 5f;

    void Awake() {
        GMS = GameObject.Find("GameManager").GetComponent<GameManager>();
        GMS.Current_coins++;
    }

    void Start() {
    
	}

    // Update is called once per frame
    void Update() {
        transform.Rotate(Vector3.right * RotateSpeed);
	}

    void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			Destroy (gameObject);
			GMS.Current_coins--;
			GMS.UpdateUI ();
		}
	}
}
