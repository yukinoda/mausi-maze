﻿using UnityEngine;
using System.Collections;

public class DestroyEnemy : MonoBehaviour {
	
    GameManager GMS;
    public Transform Shield;

    void Awake() {
        GMS = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void OnTriggerEnter(Collider coll) {
		if (coll.tag == "Shield") {
			GMS.Current_shields--;
			GMS.UpdateUI ();
			Destroy (gameObject);
			Shield.GetComponent<MeshRenderer> ().enabled = false;
			Shield.GetComponent<BoxCollider> ().enabled = false;
		}
	}
}
