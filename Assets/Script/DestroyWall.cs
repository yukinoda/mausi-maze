﻿using UnityEngine;
using System.Collections;

public class DestroyWall : MonoBehaviour {
	
    GameManager GMS;
    
    public Rigidbody projectile;
    public float speed = 20;

    void Awake() {
		GMS = GameObject.Find ("GameManager").GetComponent<GameManager> ();

	}

    void FixedUpdate() {
        //keybordinput
        if (Input.GetKey(KeyCode.Space)) {
            Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position, transform.rotation)
            as Rigidbody;
            //make the object move
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(speed, 0, 0));
            Destroy(gameObject);
            GMS.Current_picks--;
            GMS.UpdateUI();
        }
    }
}
