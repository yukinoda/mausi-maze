﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

    public Text CoinsLeft;
    public Text Picks;
    public Text Lifes;
    public Text Shields;

    public int Current_coins = 0;
    public int Max_coins = 0;
    public int Current_picks = 0;
    public int Current_shields = 0;
    public int playerHealth = 0;


    // Use this for initialization
    void Start () {
        playerHealth = 10;
        Max_coins = Current_coins;
        UpdateUI();
	}

	public void UpdateUI() {   
         CoinsLeft.text = "Coins left: " + Current_coins.ToString("D1") + "/" + Max_coins.ToString("D1");
         Picks.text = "Picks: " + Current_picks.ToString("D1");
         Shields.text = "Shields: " + Current_shields.ToString("D1");
         Lifes.text = "Lifes: " + playerHealth.ToString("D2");
    }
}
