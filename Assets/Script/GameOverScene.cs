﻿using UnityEngine;
using System.Collections;

public class GameOverScene : MonoBehaviour {
	
    public GUISkin layout;
  
    void OnGUI() { 
		GUI.skin = layout;

		if (GUI.Button (new Rect (575, 330, 180, 50), "Try again")) {
			Application.LoadLevel (1);
		}
			
		if (GUI.Button (new Rect (575, 430, 180, 50), "Quite")) {
			Application.Quit ();
		}
	}
}
 