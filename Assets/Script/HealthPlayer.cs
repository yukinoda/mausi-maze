﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;



public class HealthPlayer : MonoBehaviour {

    GameManager GMS;
    public int playerHealth;

    void Awake() {
		GMS = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
    
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Snowman" || other.gameObject.tag == "Bear") {
			GMS.playerHealth--;
			GMS.UpdateUI ();     
		}
	}

	void Update() {
		if (GMS.playerHealth == 0) {
			SceneManager.LoadScene ("MazeScene3_GameOver");
		}
	}
}