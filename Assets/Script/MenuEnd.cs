﻿using UnityEngine;
using System.Collections;

public class MenuEnd : MonoBehaviour {
    public GUISkin layout;

    void OnGUI() {
		GUI.skin = layout;

		if (GUI.Button (new Rect (575, 330, 180, 50), "Select Level")) {
			Application.LoadLevel (0);
		}

		if (GUI.Button (new Rect (575, 430, 180, 50), "End")) {
			Application.LoadLevel (3);
		}  
	}
}
