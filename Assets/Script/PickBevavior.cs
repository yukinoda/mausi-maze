﻿using UnityEngine;
using System.Collections;

public class PickBevavior : MonoBehaviour {
    	
    GameManager GMS;
      
    void Awake() {
		GMS = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

    void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			GMS.Current_picks++;
			GMS.UpdateUI ();
		}
		if (other.gameObject.tag == "Wall") {
			GMS.Current_picks--;
			GMS.UpdateUI ();
		}
	}
}
