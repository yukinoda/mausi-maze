﻿using UnityEngine;
using System.Collections;

public class SchieldCounter : MonoBehaviour {

    GameManager GMS;

    void Awake(){
		GMS = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
		
    void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			GMS.Current_shields++;
			GMS.UpdateUI ();
		}
	}
}
