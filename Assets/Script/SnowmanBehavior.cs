﻿using UnityEngine;
using System.Collections;

public class SnowmanBehavior : MonoBehaviour {
	
    public float RotateSpeed = 5f;
    // Use this for initialization

    void Start () {
	
	}

    void Update() {
		transform.Rotate (Vector3.up * RotateSpeed);
	}
}
