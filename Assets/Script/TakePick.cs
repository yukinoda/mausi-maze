﻿using UnityEngine;
using System.Collections;

public class TakePick : MonoBehaviour {

    public Transform Pick;

    void OnTriggerEnter(Collider coll) {
		if (coll.CompareTag ("Player")) {
			Pick.GetComponent<MeshRenderer> ().enabled = true;
			Pick.GetComponent<DestroyWall> ().enabled = true;
			Destroy (gameObject); 
		}
	}
}
