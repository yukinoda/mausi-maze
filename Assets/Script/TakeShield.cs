﻿using UnityEngine;
using System.Collections;

public class TakeShield : MonoBehaviour {

	public Transform Shield;

	void OnTriggerEnter(Collider coll) {
		if (coll.CompareTag ("Player")) {
			Shield.GetComponent<MeshRenderer> ().enabled = true;
			Shield.GetComponent<BoxCollider> ().enabled = true;

			Destroy (gameObject);
		}
	}
}
