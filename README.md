## Developing Environment
Hardware: Windows/MacOS  
Game Engine: [Unity](https://unity3d.com)  
Programming Language: C#  
3D Modeling and Animation: [Blender](https://www.blender.org)/[Maya](https://www.autodesk.com/products/maya/overview)

## Description
Mausi Maze is an adventure and strategy game which will be inspired in a traditional maze, a puzzle where the user must find the exit. In this game the main character, a mouse, will travel through different worlds with many levels designed with branching corridors with only one exit. The player can collect different tools that help to find the right way out and defend himself from enemies as well
as coins and items.